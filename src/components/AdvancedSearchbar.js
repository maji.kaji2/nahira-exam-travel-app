import React from "react";
import {
  Form,
  Button,
  Row,
  Col,
  Container,
  Accordion,
  Card,
} from "react-bootstrap";

const AdvancedSearchbar = () => (
  <div className="advanced-search ">
    <Container>
      <Row>
        <Col>
          <Accordion defaultActiveKey="0">
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  <i class="fa fa-plane"></i> Airplane
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <Form.Control as="select">
                    <option>travel: 1</option>
                    <option>travel: 2</option>
                    <option>travel: 3</option>
                    <option>travel: 4</option>
                  </Form.Control>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Col>
        <Col>
          <Accordion defaultActiveKey="0">
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  <i class="fa fa-train"></i> Train
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <Form.Control as="select">
                    <option>travel: 1</option>
                    <option>travel: 2</option>
                    <option>travel: 3</option>
                    <option>travel: 4</option>
                  </Form.Control>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Col>
        <Col>
          <Accordion defaultActiveKey="0">
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  <i class="fa fa-bus"></i> Bus
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <Form.Control as="select">
                    <option>travel: 1</option>
                    <option>travel: 2</option>
                    <option>travel: 3</option>
                    <option>travel: 4</option>
                  </Form.Control>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Col>
        <Accordion defaultActiveKey="0">
          <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="0">
                <i class="fa fa-hotel"></i> Hotel
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
                <Form.Control as="select">
                  <option>travel: 1</option>
                  <option>travel: 2</option>
                  <option>travel: 3</option>
                  <option>travel: 4</option>
                </Form.Control>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </Row>
      <Row className={"advanced-search-text"}>
        <div>
          <h1>advanced search</h1>
        </div>
      </Row>

      <Row>
        <Col>
          {" "}
          <button className="btn-advnced-search">search country</button>
          <button className="btn-advnced-search">search city</button>
        </Col>
        <Col>
          {" "}
          <button className="btn-advnced-search">search country</button>
          <button className="btn-advnced-search">search city</button>
        </Col>
        <Col>
          {" "}
          <button className="btn-advnced-search">search country</button>
          <button className="btn-advnced-search">search city</button>
        </Col>
      </Row>
    </Container>
  </div>
);

export default AdvancedSearchbar;
