import React from "react";
import { Form, Button, Row, Col, Container } from "react-bootstrap";

const Footer = () => (
  <Container className="footer">
    <Row className="social-icons-footer">
      <Col>
        <a href="#" class="fa fa-facebook"></a>
      </Col>
      <Col>
        <a href="#" class="fa fa-twitter"></a>
      </Col>
      <Col>
        <a href="#" class="fa fa-instagram"></a>
      </Col>
      <Col>
        <a href="#" class="fa fa-soundcloud"></a>
      </Col>
      <Col>
        <a href="#" class="fa fa-gitlab"></a>
      </Col>
      <Col>
        <a href="#" class="fa fa-github"></a>
      </Col>
    </Row>
    <Row className="text-footer">
      <Col>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit
        nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar
        aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet
        porttitor venenatis. Donec a dui et dui fringilla consectetur id nec
        massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel
        tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis.
        Duis nunc eros, mattis at dui ac, convallis semper risus. In adipiscing
        ultrices tellus, in suscipit massa vehicula eu.
      </Col>
      <Col>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit
        nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar
        aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet
        porttitor venenatis. Donec a dui et dui fringilla consectetur id nec
        massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel
        tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis.
        Duis nunc eros, mattis at dui ac, convallis semper risus. In adipiscing
        ultrices tellus, in suscipit massa vehicula eu.
      </Col>
      <Col>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit
        nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar
        aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet
        porttitor venenatis. Donec a dui et dui fringilla consectetur id nec
        massa. Aliquam erat volutpat. Sed ut dui ut lacus dictum fermentum vel
        tincidunt neque. Sed sed lacinia lectus. Duis sit amet sodales felis.
        Duis nunc eros, mattis at dui ac, convallis semper risus. In adipiscing
        ultrices tellus, in suscipit massa vehicula eu.
      </Col>
    </Row>
    <Row className="footer-bottom-row">
      <Col>copyright Nahira 2020</Col>
      <Col>copyright Nahira 2020</Col>
      <Col>copyright Nahira 2020</Col>
    </Row>
  </Container>
);

export default Footer;
