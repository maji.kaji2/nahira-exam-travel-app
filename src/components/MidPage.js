import React from "react";
import { Carousel } from "react-bootstrap";
import iran1 from "../styles/assets/iran1.jpg";
import iran2 from "../styles/assets/iran2.jpg";
import iran3 from "../styles/assets/iran3.jpg";
import { Form, Button, Row, Col, Container } from "react-bootstrap";
import newyork from "../styles/assets/newYork.jpeg";
import spain from "../styles/assets/spain park.jpeg";
import paris from "../styles/assets/paris.jpeg";

import PicLuggage from "../styles/assets/luggage.png";

import BtmPicBritain from "../styles/assets/bottom-pics-britain.png";
import BtmPicChina from "../styles/assets/bottom-pics-china.png";
import BtmPicMount from "../styles/assets/bottom-pics-mount.png";
import BtmPicParis from "../styles/assets/bottom-pics-paris.png";
import BtmPicPark from "../styles/assets/bottom-pics-park.png";
import BtmPicPizzaTower from "../styles/assets/bottom-pics-pizza-tower.png";

const MidPage = () => (
  <div className="midpage">
    {" "}
    <Carousel>
      <Carousel.Item>
        <img className="midpage-carousel" src={iran1} alt="iran1" />
        <Carousel.Caption>
          <h3>Yaz</h3>
          <p>Yazd, Iran is .......</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="midpage-carousel" src={iran2} alt="iran2" />
        <Carousel.Caption>
          <h3>Esfahan</h3>
          <p>Esfahan, Iran is ........</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="midpage-carousel" src={iran3} alt="iran3" />
        <Carousel.Caption>
          <h3>Shiraz</h3>
          <p>Shiraz, Iran is .....</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    <Carousel>
      <Carousel.Item>
        <img className="midpage-carousel" src={newyork} alt="new york" />
        <Carousel.Caption>
          <h3>New York</h3>
          <p>New York, USA is.......</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="midpage-carousel" src={paris} alt="Paris" />
        <Carousel.Caption>
          <h3>Paris</h3>
          <p>Paris, France is.......</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="midpage-carousel" src={spain} alt="spain" />
        <Carousel.Caption>
          <h3>Spain</h3>
          <p>Spain is...........</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    <Container className="midPage-search">
      <Row>
        <Col className="midPage-search-btn-form-text">
          <Row className="midPage-text">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
              hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus
              rhoncus pulvinar aliqua
            </p>
          </Row>
          <Row className="midPage-search-btn-form">
            <Col>
              <button className="midPage-search-btn"> Search </button>
            </Col>
            <Col>
              <Form.Control type="text" placeholder="Search Here" />
            </Col>
          </Row>
        </Col>

        <Col>
          <img src={PicLuggage} alt="PicLuggage" />
        </Col>
      </Row>
    </Container>
    <div>
      {" "}
      <img src={BtmPicBritain} alt={BtmPicBritain} className="midPageBtmPics" />
      <img src={BtmPicChina} alt={BtmPicChina} className="midPageBtmPics" />
      <img src={BtmPicMount} alt={BtmPicMount} className="midPageBtmPics" />
      <img src={BtmPicParis} alt={BtmPicParis} className="midPageBtmPics" />
      <img src={BtmPicPark} alt={BtmPicPark} className="midPageBtmPics" />
      <img src={BtmPicPizzaTower} alt={BtmPicPizzaTower} />
    </div>
  </div>
);

export default MidPage;
