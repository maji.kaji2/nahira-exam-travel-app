import React from "react";
import backgroundNavbar from "../styles/assets/airplane.png";
import { Button, ButtonGroup } from "react-bootstrap";

const MyNavbar = () => (
  <div
    style={{
      backgroundImage: "url(" + backgroundNavbar + ")",
      backgroundPositionX: "center",
      width: "device-width",
      backgroundRepeat: "no-repeat",
      justifyContent: "center",
      height: 330,
      textAlign: "center",
    }}
  >
    <ButtonGroup>
      <button className="navbar-l">Left btn</button>
      <button className="navbar">Middle 1</button>
      <button className="navbar">Middle 2</button>
      <button className="navbar">Middle 3</button>
      <button className="navbar">Middle 4</button>
      <button className="navbar-r">Right btn</button>
    </ButtonGroup>
  </div>
);

export default MyNavbar;
