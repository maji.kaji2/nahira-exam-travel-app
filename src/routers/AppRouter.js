import React from "react";
import AdvancedSearchbar from "../components/AdvancedSearchbar";
import Footer from "../components/Footer";
import MyNavbar from "../components/Navbar";
import MidPage from "../components/MidPage";
import "../styles/styles.scss";
import { Form, Button, Row, Col, Container } from "react-bootstrap";

const AppRouter = () => (
  <Container>
    <MyNavbar />
    <AdvancedSearchbar />
    <MidPage />
    <Footer />
  </Container>
);

export default AppRouter;
